export const colors = {
  primary: '#4ba614',
  white: '#ffffff',
  gray: '#efefef',
  grayDark1: '#d8d8d8',
  grayDark: '#444444',
  error: 'red',

  buttons: {
    primary: '#4ba614',
    primaryDisabled: '#c0e6a9',
  },
}

export const sizes = {
  borderRadius: '3px',
  basePadding: '8px',
}

export const spacing = {
  base: '8px',
}

export const fontSize = {
  base: '1em',
  xs: '.65em',
  sm: '.85em',
  md: '1em',
  lg: '1.17em',
  xl: '1.5em',
  xxl: '2em',
}
