import styled from 'styled-components'

const StyledFormControl = styled.div`
  margin-bottom: 10px;
`

export { StyledFormControl }
