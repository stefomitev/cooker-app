import React from 'react'
import { SingleColumn } from '@shared/components/templates/SingleColumn'

const Profile: React.FC = () => (
  <SingleColumn>
    <h1>Hello Profile</h1>
  </SingleColumn>
)

export default Profile
