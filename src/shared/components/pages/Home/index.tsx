import React from 'react'
import { SingleColumn } from '@shared/components/templates/SingleColumn'

const Home: React.FC = () => (
  <SingleColumn>
    <h1>Hello Home</h1>
  </SingleColumn>
)

export default Home
