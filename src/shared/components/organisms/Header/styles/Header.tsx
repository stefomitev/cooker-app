import styled from 'styled-components'
import { colors } from '@shared/style'

const StyledHeader = styled.div`
  background: ${colors.gray};
  border: 1px solid ${colors.grayDark1};
  color: ${colors.grayDark};
`

export { StyledHeader }
