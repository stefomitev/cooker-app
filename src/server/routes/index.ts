// eslint-disable-next-line @typescript-eslint/no-unused-vars
import Router from 'koa-router'
import mainRoutes from './main'
import reactRoutes from './react'

export default [mainRoutes, reactRoutes]
