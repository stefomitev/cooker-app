declare interface Window {
  APP: {
    USER: User
    // maybe define global app state type?
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    STATE: any
  }
}
